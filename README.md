# CLEANUP!

This cleans up a few things...

## How do I do it?
1. Install
2. Activate
3. High Five

## What the heck is in here?

### Remove the ID from the menu item
<pre>
    <code>
function clear_nav_menu_item_id($id, $item, $args) {
    return "";
}
add_filter('nav_menu_item_id', 'clear_nav_menu_item_id', 10, 3);
    </code>
</pre>

### Remove the class from the menu item
<pre>
    <code>
function clear_nav_menu_item_class($classes, $item, $args) {
    return array();
}
add_filter('nav_menu_css_class', 'clear_nav_menu_item_class', 10, 3);
    </code>
</pre>

### Dequeue jQuery Migrate
<pre>
    <code>
function dequeue_jquery_migrate( &$scripts){
    if(!is_admin()){
$scripts->remove( 'jquery');
$scripts->add( 'jquery', false, array( 'jquery-core' ), '1.10.2' );
    }
}
add_filter( 'wp_default_scripts', 'dequeue_jquery_migrate' );
    </code>
</pre>

### Pesky header code bloat such as RSS links, version generator, post links etc
<pre>
    <code>
function removeHeadLinks() {
    remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
    remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
    remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
    remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
    remove_action( 'wp_head', 'index_rel_link' ); // index link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
    remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
    remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
}
add_action('init', 'removeHeadLinks');
    </code>
</pre>

### Disable XMLRPC. More of a security feature than a performance one.
<pre>
    <code>
add_filter('xmlrpc_enabled', '__return_false');
    </code>
</pre>

### If you're not likely to be using comments then you wont be using emojis.
<pre>
    <code>
function disable_wp_emojicons() {
    // ALL actions related to emojis
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    // filter to remove TinyMCE emojis
    add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );

function disable_emojicons_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
return array();
    }
}
add_filter( 'emoji_svg_url', '__return_false' );
    </code>
</pre>
